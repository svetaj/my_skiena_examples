#include <stdio.h>
#include <string.h>

typedef int item_type;

typedef struct tree {
     item_type item;
     struct tree *parent;
     struct tree *left;
     struct tree *right;
} tree;

tree *search_tree(tree *l, item_type x)
{
	if (l == NULL) return (NULL);
	if (l->item == x) return(l);

	if (x < l->item)
	    return (search_tree(l->left, x));
	else
	    return (search_tree(l->right, x));
}

tree *find_minimum(tree *t)
{
	tree *min;
	if (t == NULL) return (NULL);
	min = t;
	while (min->left != NULL)
	     min = min->left;
	return(min);
}

tree *find_maximum(tree *t)
{
	tree *max;
	if (t == NULL) return (NULL);
	max = t;
	while (max->right != NULL)
	     max = max->right;
	return(max);
}

void insert_tree(tree **l, item_type x, tree *parent)
{
	tree *p;                     /* temporary pointer */

	if (*l == NULL) {
        p = (tree *) malloc(sizeof(tree));  /* allocate new node */
        p->item = x;
        p->left = p->right = NULL;
        p->parent = parent;
        *l = p;   /* link int parent record */
        return;
    }

    if (x < (*l)->item)
        insert_tree(&((*l)->left), x, *l);
    else
        insert_tree(&((*l)->right), x, *l);
}

void delete_tree(tree *l, item_type x)
{
	tree *t, *tmin;
	tree *d;
	d = search_tree(l, x);
	if (d == NULL) return;
	t= d->parent;

	if (d->right != NULL && d->left != NULL) {  /* relabel the node with tne immidiate successor */
	    printf ("d->left != NULL && d->right != NULL\n");
		tmin = find_minimum(d->right);          /* leftmost descendant in the right subtree      */
        d->item = tmin->item;
        d = tmin;
        t = d->parent;
    }

	if (d->left == NULL && d->right == NULL) {
	    printf ("d->left == NULL && d->right == NULL\n");
		if (t != NULL) {
		   if (t->left  == d) t->left  = NULL;
		   if (t->right == d) t->right = NULL;
	    }
    }
	if (d->left != NULL && d->right == NULL) {
	    printf ("d->left != NULL && d->right == NULL\n");
		if (t != NULL) {
		   if (t->left  == d) t->left  = d->left;
		   if (t->right == d) t->right = d->left;
	    }
    }
	if (d->right != NULL && d->left == NULL) {
	    printf ("d->left == NULL && d->right != NULL\n");
		if (t != NULL) {
		    if (t->left  == d) t->left  = d->right;
		    if (t->right == d) t->right = d->right;
	    }
    }
    free(d);
}

void debug_item(tree *l)
{
	printf("--------------------\n");
	printf("l=%p\n",l);
	printf("l->item=%d\n",l->item);
	printf("l->parent=%p\n",l->parent);
	printf("l->left=%p\n",l->left);
	printf("l->right=%p\n",l->right);
}

void traverse_tree(tree *l, void (*process_item)(item_type))
{
	if (l != NULL) {
		traverse_tree(l->left, process_item);
		(*process_item)(l->item);
/*		debug_item(l);  */
		traverse_tree(l->right, process_item);
    }
}

void print_item(item_type x)
{
	printf("item %d\n", x);
}

void main(int argc, char *argv[])
{
	int i,j,op,oc;
	char *o[] = {"i","d","p"};
	tree *tt = NULL;


    op = 0;
	for (i=1; i<argc; i++) {
		oc = 0;
		for (j=0;j<3;j++) {
		    if (strcmp(argv[i], o[j]) == 0) {
		  	    oc = op = j+1;
			    break;
		    }
	    }
		if (oc != 0 && op != 3) continue;
	    switch(op) {
		    case 1:
		        printf("insert %d\n", atoi(argv[i]));
		        insert_tree (&tt, atoi(argv[i]), tt);
		        break;
		    case 2:
		        printf("delete %d\n", atoi(argv[i]));
		        delete_tree (tt, atoi(argv[i]));
		        break;
		    case 3:
		        printf("---------------------------\n");
		        printf("print tree\n");
		        printf("---------------------------\n");
		        traverse_tree (tt, print_item);
		        printf("---------------------------\n");
		        break;
         }
    }
}

