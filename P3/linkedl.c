#include <stdio.h>
typedef int item_type;

typedef struct list {
	item_type item;      /* data item */
	struct list *next;   /* pointer to successor */
} list;
list *search_list(list *, item_type), *predecessor_list(list *, item_type);
void print_list(char *, list *);

list *search_list (list *l, item_type x)
{
	if (l == NULL) return (NULL);

	if (l->item == x)
	     return (l);
	else
	     return (search_list(l->next,x));
}

void print_list (char *t, list *l)
{
	printf("---- %s ----\n",t);
	while (l != NULL) {
		printf ("Item %d\n", l->item);
		l=l->next;
    }
	printf("---- %s ----\n",t);
}

void insert_list (list **l, item_type x)
{
	list *p;                 /* temporary pointer */
	p = (list *) malloc(sizeof(list));
	p->item = x;
	p->next = *l;
	*l = p;
}

list *predecessor_list(list *l, item_type x)
{
	if ((l == NULL) || (l->next == NULL)) {
		printf("Error: predecessor sought on null list \n");
		return (NULL);
    }
    if ((l->next)->item == x)
         return (l);
    else
         return (predecessor_list(l->next, x));
}

delete_list(list **l, item_type x)
{
	list *p;       /* item pointer */
	list *pred;     /* predecessor pointer */

	p = search_list (*l,x);
	if (p != NULL) {
		printf("found %d\n",x);
	    pred = predecessor_list(*l,x);
	    if (pred == NULL)
	        *l = p->next;
	    else
	        pred->next = p->next;
	    free(p);         /* free memory used by node */
    }
}

void main()
{
	list *aaa = NULL;

	insert_list (&aaa, 100);
	insert_list (&aaa, 999);
	insert_list (&aaa, 1);
	insert_list (&aaa, 4);

    print_list ("list1", aaa);

	insert_list (&aaa, 6);
	insert_list (&aaa, 3);

    print_list ("list2", aaa);

    delete_list(&aaa, 999);

    print_list ("list3", aaa);
}