#include <stdio.h>

mmux(int a[], int b[], int c[], int x, int y, int z)
{
     int i,j,k;
     for (i=0; i<x; i++) {
        for (j=0; j<z; j++) {
            c[i*z+j] = 0;
            for (k=0; k<y; k++)
                c[i*z+j] += a[i*y+k] * b[k*y+j];
        }
     }
}

mprint (int a[], int x, int y, char *t)
{
	int i,j;
	printf("------------ %s -----------\n",t);
    for (i=0; i<x; i++) {
       for (j=0; j<y; j++)
	       printf("%d ",a[i*y+j]);
	   printf("\n");
    }
	printf("\n");
}

main(int argc, char *argv[])
{
	int x=5;
	int y=3;
	int z=4;
	int a[15] = {2,4,6,8,10,12,14,16,18,20,22,24,26,28,30};
	int b[12] = {1,3,7,9,13,15,17,19,1,1,1,1};
	int c[20];

	mprint(a,x,y, "a");
	mprint(b,y,z, "b");
	mmux(a,b,c,x,y,z);
	mprint(c,x,z, "c");
}

