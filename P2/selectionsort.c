#include <stdio.h>
typedef int item;

swap(item *x, item *y)
{
	item t;
	t = *x;
	*x = *y;
	*y = t;
}

selection_sort(item s[], int n)
{
     int i,j;         /* counters */
     item min;

     for (i=0; i<n; i++) {
        min=i;
        for (j=i+1; j<n; j++)
			if (s[j] < s[min]) min = j;
        swap (&s[i], &s[min]);

     }
}

void main(int argc, char *argv[])
{
	int i,j;
	item o[100];

    printf("--------------------------\n");
    printf("------ UNSORTED ----------\n");
    printf("--------------------------\n");
	for (i=1; i<argc; i++) {
       o[i-1] = atoi(argv[i]);
	   printf("%d ",o[i-1]);
	}
    printf("\n--------------------------\n");
    printf("-------- SORTED ----------\n");
    printf("--------------------------\n");

    selection_sort(o,argc-1);

	for (i=1; i<argc; i++) {
	   printf("%d ",o[i-1]);
	}
}