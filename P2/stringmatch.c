#include <stdio.h>

int findmatch(char *p, char *t)
{
	int i, j;    /* counters */
	int m, n;    /* string lengths  */

	m = strlen(p);
	n = strlen(t);

	for (i=0; i<=(n-m); i++) {
		j=0;
		while ((j<m) && (t[i+j] == p[j]))
		    j = j+1;
		if (j == m) return (i);
    }
    return (-1);
}

main(int argc, char *argv[])
{
	printf("text=%s\n",argv[2]);
	printf("pattern=%s\n",argv[1]);
	printf("result=%d\n",findmatch(argv[1], argv[2]));
}