#include <stdio.h>
#include <stdlib.h>

                /* ACTION                            */
#define TRAV 1  /* 1 - GRAPH TRAVERSAL               */
#define CYCL 2  /* 2 - FINDING CYCLES                */
#define ARTI 3  /* 3 - ARTICULATION VERTICES         */
#define TOPO 4  /* 4 - TOPOLOGICAL SORTING           */
#define STRO 5  /* 5 - STRONGLY CONNECTED COMPONENTS */
#define CONN 6  /* 6 - CONNECTED COMPONENTS          */
int action;

typedef int bool;
#define TRUE 1
#define FALSE 0

#define MAXV 1000000    /* maximum number of vertices */

bool processed[MAXV+1];         /* which vertices have been processed */
bool discovered[MAXV+1];        /* which vertices have been found */
int parent[MAXV+1];             /* discovery relation */
int reachable_ancestor[MAXV+1]; /* earliest reachable ancestor of v */
int tree_out_degree[MAXV+1];    /* DFS tree outdegree of v */
int low[MAXV+1];                /* oldest vertex surely in component of v */
int scc[MAXV+1];                /* strong component number for each vertex */
int components_found;

typedef struct {
	int y;                      /* adjancency info */
	int weight;                 /* edge weight, if any */
	struct edgenode *next;      /* next edge in list */
} edgenode;

typedef struct {
	edgenode *edges[MAXV+1];    /* adjancency info */
	int degree[MAXV+1];         /* outdegree of each vertex */
	int nvertices;              /* number of vertices in graph */
	int nedges;                 /* number of edges in graph */
	bool directed;              /* is it graph directed */
} graph;

int time;
int entry_time[MAXV+1];
int exit_time[MAXV+1];
int time;
bool finished;

#define TREE    301
#define BACK    302
#define FORWARD 303
#define CROSS   304

#define STACKSIZE       1000

/* LIFO - LAST IN FIRST OUT */
typedef struct {
        int s[STACKSIZE];		/* body of stack */
        int last;              /* position of first element */
} stack;

init_stack(stack *s)
{
        s->last = -1;
}

push(stack *s, int x)
{
        if (s->last == STACKSIZE-1)
		    printf("Warning: stack overflow x=%d\n",x);
        else {
                s->last = (s->last)+1;
                s->s[ s->last ] = x;
        }
}

int pop(stack *s)
{
        int x;

        if (s->last == -1)
             printf("Warning: empty stack.\n");
        else {
                x = s->s[ s->last ];
                s->last = s->last - 1;
        }

        return(x);
}


print_stack(stack *s)
{
        int i;

        printf("STACK (%d) : ", s->last+1);
        for (i=s->last; i>=0; i--) {
                printf("%d ",s->s[i]);
        }
        printf("\n");
}

stack sorted;    /* stack for topological sort */
stack active;    /* stack for strongly conected components */

initialize_graph(graph *g, bool directed)
{
	int i;           /* counter */

	g->nvertices = 0;
	g->nedges = 0;
	g->directed = directed;

	for (i=1; i<=MAXV; i++) g->degree[i] = 0;
	for (i=1; i<=MAXV; i++) g->edges[i] = NULL;
}

insert_edge(graph *g, int x, int y, bool directed)
{
	edgenode *p;                    /* temporary pointer */

	p = malloc(sizeof(edgenode));   /* allocate edgenode storage */

	p->weight = NULL;
	p->y = y;
	p->next = g->edges[x];

	g->edges[x] = p;                /* insert at head of list */
	g->degree[x] ++;

	if (directed == FALSE)
	    insert_edge(g,y,x,TRUE);
	else
	    g->nedges ++;
}


read_graph(graph *g, bool directed)
{
	int i;                /* counter */
	int m;                /* number of edges */
	int x, y;             /* vertices in edge (x,y) */

	initialize_graph(g, directed);

	scanf("%d %d",&(g->nvertices),&m);

	for (i=1; i<=m; i++) {
	     scanf("%d %d",&x,&y);
	     insert_edge(g,x,y,directed);
    }
}


print_graph(graph *g)
{
	int i;            /* counter */
	edgenode *p;      /* temporary pointer */

	for (i=1; i<=g->nvertices; i++) {
	    printf("GRAPH %d ", i);
	    p = g->edges[i];
	    while (p != NULL) {
			printf(" %d", p->y);
			p = p->next;
	    }
	    printf("\n");
    }
}

initialize_search(graph *g)
{
	int i;               /* counter */

    time = 0;
    finished = FALSE;
	for (i=1; i<=g->nvertices; i++) {
		processed[i] = discovered[i] = FALSE;
		parent[i] = -1;
		entry_time[i] = 0;
		exit_time[i] = 0;
    }
}

int edge_classification(int x, int y)
{
	 if (parent[y] == x) return (TREE);
	 if (discovered[y] == TRUE && processed[y] == FALSE) return(BACK);
	 if (processed[y] == TRUE && entry_time[y] > entry_time[x]) return(FORWARD);
	 if (processed[y] == TRUE && entry_time[y] < entry_time[x]) return(CROSS);

     printf("Warning: unclassified edge (%d, %d)\n",x,y);
}

pop_component(int v)
{
	int t;           /* vertex placeholder */

	components_found = components_found + 1;

	scc[ v ] = components_found;
	while ((t = pop(&active)) != v) {
		scc[ t ] = components_found;
    }
}

process_vertex_late(int v)
{
	bool root;         /* is the vertex the root of the DFS tree? */
	int time_v;        /* earliest reachable time for v */
	int time_parent;   /* earliest reachable time for parent[v] */

    if (low[v] == v) {    /* edge (parent[v],v) cuts off acc */
         pop_component(v);
    }

    if (entry_time[low[v]] < entry_time[low[parent[v]]])
        low[parent[v]] = low[v];

    push(&sorted,v);

    if (action != 3) return;

	if (parent[v] < 1) {   /* test if v is root */
	    if (tree_out_degree[v] > 1)
	         printf("########## root articulation vertex: %d \n",v);
             return;
    }

    if (parent[parent[v]] < 1) root = TRUE;           /*  is parent[v] the root? */
    if (reachable_ancestor[v] == parent[v] && root != TRUE)
        printf("########## parent articulation vertex: %d\n", parent[v]);

    if (reachable_ancestor[v] == v) {
		printf("########## bridge articulation vertex: %d\n", parent[v]);
		if (tree_out_degree[v] > 0)                   /* test if v is not leaf */
		    printf("########## bridge articulation vertex: %d \n",v);
	}

	time_v = entry_time[ reachable_ancestor[v] ];
	time_parent = entry_time[ reachable_ancestor[parent[v]] ];

	if (time_v < time_parent)
	    reachable_ancestor[parent[v]] = reachable_ancestor[v];
}

process_vertex_early(int v)
{
	printf("processed vertex %d\n",v);
	reachable_ancestor[v] = v;

	push(&active, v);
}

process_edge(int x, int y)
{
	int class;                                 /* edge class */

	class = edge_classification(x,y);

	if (class == TREE && action == 3)
	   tree_out_degree[x] = tree_out_degree[x] + 1;

	if (class == BACK) {
	   if (entry_time[y] < entry_time[ low[x] ])
	       low[x] = y;
	   if (parent[x] != y && action == 3) {
	       printf("Warning: directed cycle found, not a DAG\n");
	       if (entry_time[y] < entry_time[ reachable_ancestor[x] ])
	            reachable_ancestor[x] = y;
       }
    }

	if (class == CROSS) {
	   if (scc[y] == -1)     /* component not yet assigned */
	       if (entry_time[y] < entry_time[ low[x] ])
	            low[x] = y;
    }

	printf("processed edge (%d, %d)\n",x,y);

    if (action == CYCL)
	    if (parent[x] != y) {                      /* found back edge */
	        printf("------- Cycle from %d to %d:", y, x);
	        find_path(y,x,parent);
	        printf("\n\n");
	        finished = TRUE;
        }
}

topsort(graph *g)
{
	int i;             /* counter */

	init_stack(&sorted);

	for (i=1; i<=g->nvertices; i++)
	    if (discovered[i] == FALSE)
	        dfs(g,i);

	print_stack(&sorted);    /* report topological order */
}

strong_components(graph *g)
{
	int i;              /* counter */

	for (i=1; i<=(g->nvertices); i++) {
		low[i] = i;
		scc[i] = -1;
    }
    components_found = 0;
    init_stack(&active);

    for (i=1; i<=(g->nvertices); i++)
        if (discovered[i] == FALSE) {
			dfs(g,i);
	    }
}

print_scc (graph *g)
{
	int i;              /* counter */

	for (i=1; i<=(g->nvertices); i++)
	    printf("SCC low[%d]=%d, scc[%d]=%d\n", i, low[i], i, scc[i]);
}

print_ra (graph *g)
{
	int i;              /* counter */

	for (i=1; i<=(g->nvertices); i++)
       printf("reachable_ancestor[%d]=%d tree_out_degree[%d]=%d\n",i,reachable_ancestor[i],i,tree_out_degree[i]);
}

dfs(graph *g, int v)                /* DEPTH-FIRST SEARCH */
{
	edgenode *p;                    /* temporary pointer */
	int y;                          /* successor vertex */

    if (finished == TRUE) return;   /* allow for search termination */

	discovered[v] = TRUE;
	time = time + 1;
	entry_time[v] = time;

	process_vertex_early(v);

	p = g->edges[v];
    while(p != NULL) {
		y = p->y;
		if (discovered[y] == FALSE) {
            parent[y] = v;
            process_edge(v,y);
            dfs(g,y);
		}
		else if (processed[y] != TRUE || g->directed == TRUE)
		    process_edge(v,y);

		if (finished == TRUE) return;
		p = p->next;
	}
	process_vertex_late(v);
	time = time + 1;
	exit_time[v] = time;
	processed[v] = TRUE;
}


find_path(int start, int end, int parents[])
{
	if ((start == end) || (end == -1))
	     printf("\n%d",start);
	else {
		find_path(start,parents[end], parents);
		printf(" %d", end);
    }
}

connected_components(graph *g)
{
	int c;              /* component number */
	int i;              /* counter */

	c = 0;
	for (i=1; i<=g->nvertices; i++)
	    if (discovered[i] == FALSE) {
			c = c+1;
			printf("\n############ Component %d:\n",c);
			dfs(g,i);
			printf("\n");
		}
}

main(int argc, char *argv[])
{
	graph g;
	int i, ix;
	bool bx;    /* bx - directed or undirected */
	action = TRAV;

    if (argc == 2) action = atoi(argv[1]);

	printf("=============== DFS graph type =================\n");
	scanf("%d",&ix);
	bx = FALSE;
	if (ix == 1) bx = TRUE;
	if (bx == TRUE)  printf("DIRECTED GRAPH"); else printf("UNDIRECTED GRAPH");
    printf(" ACTION=%d\n", action);

	printf("========== adjancency vertices =============\n");
	read_graph(&g,bx);
	print_graph(&g);

    if (action == 1) {
	     printf("===========  DFS graph traversal ===========\n");
	     initialize_search(&g);
	     dfs(&g,1);

	     printf("=========== find path using DFS ============\n");
         for (i=1; i<=g.nvertices; i++)
             find_path(1,i,parent);
         printf("\n");
    }

    if (action == 6 || action == 2 || action == 3) {
	    if (action == 6) printf("======= connected components using DFS ========\n");
	    if (action == 2) printf("======= find cycles using DFS ========\n");
	    if (action == 3) printf("======= articulation vertices ========\n");
	    initialize_search(&g);
        connected_components(&g);
        if (action == 3) print_ra(&g);
    }

    if (bx != TRUE) return;

    if (action == 4) {
	    printf("======= topological sort using DFS =========\n");
	    initialize_search(&g);
        topsort(&g);
    }

    if (action == 5) {
	    printf("======= strong components using DFS ========\n");
	    initialize_search(&g);
        strong_components(&g);
        print_scc(&g);
    }
}




