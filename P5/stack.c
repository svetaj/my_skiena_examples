#define STACKSIZE       1000

/* LIFO - LAST IN FIRST OUT */
typedef struct {
        int s[STACKSIZE];		/* body of stack */
        int last;              /* position of first element */
} stack;

init_stack(stack *s)
{
        s->last = -1;
}

push(stack *s, int x)
{
        if (s->last == STACKSIZE-1)
		    printf("Warning: stack overflow x=%d\n",x);
        else {
                s->last = (s->last+1);
                s->s[ s->last ] = x;
        }
}

int pop(stack *s)
{
        int x;

        if (s->last == -1)
             printf("Warning: empty stack.\n");
        else {
                x = s->s[ s->last ];
                s->last = s->last - 1;
        }

        return(x);
}


print_stack(stack *s)
{
        int i;

        for (i=0; i<=s->last; i++) {
                printf("%c ",s->s[i]);
        }
        printf("\n");
}
