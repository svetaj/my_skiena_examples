#include <stdio.h>

void reverse(char *str)
{
   if(*str !='\0') {
      reverse(str+1);
      printf("%c",*str);
   }
}

main()
{
    char x[10] = "abcdefgh";

    printf("ORIGINAL %s\n",x);
    printf("REVERSED ");
    reverse(x);
}