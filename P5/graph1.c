#include <stdio.h>
#include <stdlib.h>

typedef int bool;
#define TRUE 1
#define FALSE 0

#define MAXV 1000    /* maximum number of vertices */

bool processed[MAXV+1];         /* which vertices have been processed */
bool discovered[MAXV+1];        /* which vertices have been found */
int parent[MAXV+1];             /* discovery relation */

typedef struct {
	int y;                      /* adjancency info */
	int weight;                 /* edge weight, if any */
	struct edgenode *next;      /* next edge in list */
} edgenode;

typedef struct {
	edgenode *edges[MAXV+1];    /* adjancency info */
	int degree[MAXV+1];         /* outdegree of each vertex */
	int nvertices;              /* number of vertices in graph */
	int nedges;                 /* number of edges in graph */
	bool directed;              /* is it graph directed */
} graph;

initialize_graph(graph *g, bool directed)
{
	int i;           /* counter */

	g->nvertices = 0;
	g->nedges = 0;
	g->directed = directed;

	for (i=1; i<=MAXV; i++) g->degree[i] = 0;
	for (i=1; i<=MAXV; i++) g->edges[i] = NULL;
}

read_graph(graph *g, bool directed)
{
	int i;                /* counter */
	int m;                /* number of edges */
	int x, y;             /* vertices in edge (x,y) */

	initialize_graph(g, directed);

	scanf("%d %d",&(g->nvertices),&m);

	for (i=1; i<=m; i++) {
	     scanf("%d %d",&x,&y);
	     insert_edge(g,x,y,directed);
    }
}

insert_edge(graph *g, int x, int y, bool directed)
{
	edgenode *p;                    /* temporary pointer */

	p = malloc(sizeof(edgenode));   /* allocate edgenode storage */

	p->weight = NULL;
	p->y = y;
	p->next = g->edges[x];

	g->edges[x] = p;                /* insert at head of list */
	g->degree[x] ++;

	if (directed == FALSE)
	    insert_edge(g,y,x,TRUE);
	else
	    g->nedges ++;
}


print_graph(graph *g)
{
	int i;            /* counter */
	edgenode *p;      /* temporary pointer */

	for (i=1; i<=g->nvertices; i++) {
	    printf("%d ", i);
	    p = g->edges[i];
	    while (p != NULL) {
			printf(" %d", p->y);
			p = p->next;
	    }
	    printf("\n");
    }
}

initialize_search(graph *g)
{
	int i;               /* counter */

	for (i=1l i<=g->nvertices; i++) {
		processed[i] = discovered[i] = FALSE;
		parent[i] = -1;
    }
}


bfs(graph *g, int start)     /* BREADTH-FIRST SEARCH */
{
	queue q;                 /* queue of vertices to visit */
	int v;                   /* current vertex */
	int y;                   /* successor vertex */
	edgenode *p;             /* temporary pointer */

	init_queue(&q);
	enqueue(&q, start);
	discovered[start] = TRUE;



main(int argc, char *argv[])
{
	graph g;

	read_graph(&g, FALSE);
	print_graph(&g);
}