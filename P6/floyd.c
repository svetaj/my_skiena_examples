#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define MAXINT 100007
typedef int bool;
#define TRUE 1
#define FALSE 0

#define MAXV 100    /* maximum number of vertices */

typedef struct {
	int weight[MAXV+1][MAXV+1];       /* adjacency/weight info */
	int nvertices;                    /* number of vertices in graph */
} adjacency_matrix;

initialize_adjm(adjacency_matrix *g)
{
	int i,j;           /* counter */

	g->nvertices = 0;
	for (i=1; i<=MAXV; i++)
	   for (j=1; j<=MAXV; j++)
	       g->weight[i][j] = MAXINT;
}

read_adjm(adjacency_matrix *g, bool directed)
{
	int i;                /* counter */
	int m;                /* number of edges */
	int x, y, w;          /* vertices in edge (x,y) and weight */

	scanf("%d %d",&(g->nvertices),&m);

	for (i=1; i<=m; i++) {
	     scanf("%d %d %d",&x,&y,&w);
	     g->weight[x][y] = w;
	     if (directed == FALSE) g->weight[y][x] = w;
    }
}

print_adjm(adjacency_matrix *g)
{
	int i,j;            /* counter */

	for (i=1; i<=g->nvertices; i++) {
	    for (j=1; j<=g->nvertices; j++)
             printf("| %10d ",g->weight[i][j]);
        printf(" |\n");
    }
}


floyd(adjacency_matrix *g)
{
	int i,j;          /* dimension counters */
	int k;            /* intermediate vertex counter */
	int through_k;    /* distance through vertex k */

	for (k=1; k<=g->nvertices; k++)
	    for (i=1; i<=g->nvertices; i++)
	          for (j=1; j<=g->nvertices; j++) {
				  through_k = g->weight[i][k]+g->weight[k][j];
				  if (through_k < g->weight[i][j])
				          g->weight[i][j] = through_k;
			  }
}

print_graph(adjacency_matrix *g)
{
	int i,j;			/* counters */

	for (i=1; i<=g->nvertices; i++) {
		printf("%d: ",i);
		for (j=1; j<=g->nvertices; j++)
			if (g->weight[i][j] < MAXINT)
				printf(" %d",j);
		printf("\n");
	}
}

main(int argc, char *argv[])
{
	adjacency_matrix g;

	int ix;
	bool bx;    /* bx - directed or undirected */

	printf("=============== DFS graph type =================\n");
	scanf("%d",&ix);
	bx = FALSE;
	if (ix == 1) bx = TRUE;
	if (bx == TRUE)  printf("DIRECTED GRAPH\n"); else printf("UNDIRECTED GRAPH\n");

	printf("========== initialize graph =============\n");
	initialize_adjm(&g);
	printf("========== read graph =============\n");
	read_adjm(&g,bx);
	printf("========== print graph =============\n");
	print_graph(&g);
	printf("========== print adjacency matrix =============\n");
	print_adjm(&g);
	printf("========== FLOYD min spanning tree =============\n");
	floyd(&g);
	printf("========== print graph =============\n");
	print_adjm(&g);
}

