#include <stdio.h>
#include <stdlib.h>

typedef int bool;
#define TRUE 1
#define FALSE 0

typedef int data;

bool finished = FALSE;            /* found all solutions yet ? */

#define MAXCANDIDATES 100
#define NMAX 10


bool is_a_solution(int a[], int k, int n)
{
	return((k == n)? TRUE : FALSE);        /* is k == n? */
}

construct_candidates(int a[], int k, int n, int c[], int *ncandidates)
{
	c[0] = 1;
	c[1] = 2;
	c[2] = 3;
	c[3] = 4;
	c[4] = 5;
	*ncandidates = 5;
}

make_move(int a[], int k, data input)
{
}

unmake_move(int a[], int k, data input)
{
}

process_solution(int a[], int k, data input)
{
	int i;            /* counter */

	printf("{");
	for (i=1; i<=k; i++)
	    printf(" %d", a[i]);

    printf(" }\n");
}

print_current (int a[], int n, int l, int k)
{
	int i;
	for (i=1; i<=l; i++)
	   printf("\t");
	printf("(%d): ",k);
	for (i=1; i<=n; i++)
	   printf("| %d ", a[i]);
	printf("|\n");
}

int level=0;

backtrack (int a[], int k, data input)
{
	int c[MAXCANDIDATES];      /* candidates for next position  */
	int ncandidates;           /* next position candidate count */
	int i;                     /* counter                       */

    level++;
    print_current(a,input,level,k);

	if (is_a_solution(a,k,input) == TRUE)
	        process_solution(a,k,input);
	else {
		k = k+1;
		construct_candidates(a,k,input,c,&ncandidates);
		for (i=0; i<ncandidates; i++) {
			a[k] = c[i];
			make_move(a,k,input);
			backtrack(a,k,input);
			unmake_move(a,k,input);
			if (finished == TRUE) {level--; return; }   /* terminate early */
		}
	}
	level--;
}

generate_subsets(int n)
{
	int a[NMAX];        /* solution vector */

	backtrack(a,0,n);
}

main(int argc, char *argv[])
{
	int n;
	if (argc != 2) return;
 	n = atoi(argv[1]);
	printf("n=%d\n", n);
	if (n > 0 && n <= NMAX)
	    generate_subsets(n);
}