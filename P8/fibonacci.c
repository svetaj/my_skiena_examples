#include <stdio.h>

long fib_r(int n)
{
	if (n == 0) return(0);
	if (n == 1) return(1);

	return(fib_r(n-1) + fib_r(n-2));
}

#define MAXN 45          /* largest interesting n */
#define UNKNOWN -1       /* contents denote an empty cell */
long f[MAXN+1];          /* array for caching computed fib values */

long fib_c(int n)
{
	if (f[n] == UNKNOWN)
	     f[n] = fib_c(n-1) + fib_c(n-2);

	return(f[n]);
}

long fib_c_driver(int n)
{
	int i;        /* counter */

	f[0] = 0;
	f[1] = 1;
	for (i=2; i<=n; i++) f[i] = UNKNOWN;

	return(fib_c(n));
}

long fib_dp(int n)
{
	int i;                  /* counter */
	long f[MAXN+1];         /* array to cache computed fib values */

	f[0] = 0;
	f[1] = 1;

	for (i=2; i<=n; i++) f[i] = f[i-1] + f[i-2];

	return(f[n]);
}


long fib_ultimate(int n)
{
	int i;                         /* counter                 */
	long back2=0, back1=1;         /* last two values of f[n] */
	long next;                     /* placeholder for sum     */

	if (n == 0) return (0);

	for (i=2; i<n; i++) {
		next = back1 + back2;
		back2 = back1;
		back1 = next;
	}
	return(back1+back2);
}

main(int argc, char*argv[])
{
	int i;
    long j;
	i= atoi(argv[1]);

	printf("fib_r START\n");
    j = fib_r(i);
	printf("fib_r END, result = %d\n",j);

	printf("fib_c START\n");
	fib_c_driver(i);
    j = fib_c(i);
	printf("fib_c END, result = %d\n",j);

	printf("fib_dp START\n");
    j = fib_dp(i);
	printf("fib_dp END, result = %d\n",j);

	printf("fib_ultimate START\n");
    j = fib_ultimate(i);
	printf("fib_ultimate END, result = %d\n",j);
}
