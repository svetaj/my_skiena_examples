
#include <stdio.h>
#include <stdlib.h>

#define MAXINT 10000
#define MAXN 100
#define MAXK 30
#define MAX(a,b) (((a)>(b))?(a):(b))

reconstruct_partition (int s[], int d[MAXN+1][MAXK+1], int n, int k)
{
	if (k == 1)
	    print_books(s,1,n);
	else {
		reconstruct_partition(s,d,d[n][k],k-1);
		print_books(s,d[n][k]+1,n);
	}
}

print_books(int s[], int start, int end)
{
	int i;          /* counter */

	for (i=start; i<=end; i++)
	     printf(" %d", s[i]);
	printf("\n");
}

partition(int s[], int n, int k)
{
	int m[MAXN+1][MAXK+1];     /* DP table for values   */
	int d[MAXN+1][MAXK+1];     /* DP table for dividers */
	int p[MAXN+1];             /* prefix sums array     */
	int cost;                  /* test split cost       */
	int i,j,x;                 /* counters              */

	p[0] = 0;                  /* construct prefix sums */
	for (i=1; i<=n; i++) p[i] = p[i-1] + s[i];

	for (i=1; i<=n; i++) m[i][1] = p[i];       /* initialize boundaries    */
	for (j=1; j<=k; j++) m[1][j] = s[1];

    for (i=2; i<=n; i++) {                     /* evaluate main recurrence */
        for (j=2; j<=k; j++) {
			m[i][j] = MAXINT;
            for (x=1; x<=(i-1); x++) {
				cost = MAX (m[x][j-1] , p[i]-p[x]);
				if (m[i][j] > cost) {
					   m[i][j] = cost;
					   d[i][j] = x;
				}
			}
	    }
	}
	printf("--------- partitions ------------\n");
	reconstruct_partition(s,d,n,k);     /* print book partition */

    printf("--------- m ------------\n");
    for (i=1; i<=n; i++) {
        for (j=1; j<=k; j++)
             printf ("%d ",m[i][j]);
        printf("\n");
    }
    printf("--------- d ------------\n");
    for (i=1; i<=n; i++) {
        for (j=1; j<=k; j++)
             printf ("%d ",d[i][j]);
        printf("\n");
    }
    printf("--------- p ------------\n");
    for (i=1; i<=n; i++)
        printf ("%d ",p[i]);
    printf("\n");
}

main(int argc, char *argv[])
{
	int i,n,k;
	int s[MAXN+1];

	k=atoi(argv[1]);
	n = 0;
	for (i=2; i<argc; i++) {
	    n++;
	    if (n>MAXN+1) break;
	    s[n] = atoi(argv[i]);
    }
	printf("--------------------\n");
    printf("n=%d k=%d\n",n,k);
	for (i=1; i<=n; i++)
	    printf ("%d ",s[i]);
	printf("\n");
	printf("--------------------\n");
    partition(s,n,k);
}