#include <stdio.h>
#include <string.h>

#define MATCH  0        /* enumerated type symbol for match  */
#define INSERT 1        /* enumerated type symbol for insert */
#define DELETE 2        /* enumerated type symbol for delete */

int match(char c, char d)
{
	if (c == d) return(0);
	else return(1);
}

int indel(char c)
{
	return(1);
}

int string_compare(char *s, char *t, int i, int j)
{
	int k;              /* counter */
	int opt[3];         /* cost of the three options */
	int lowest_cost;    /* lowest cost */

	if (i == 0) return (j * indel(' '));
	if (j == 0) return (i * indel(' '));

	opt[MATCH]  = string_compare(s,t,i-1,j-1) + match(s[i],t[j]);
	opt[INSERT] = string_compare(s,t,i,j-1) + indel(t[j]);
	opt[DELETE] = string_compare(s,t,i-1,j) + indel(s[i]);

	lowest_cost = opt[MATCH];
	for (k=INSERT; k<=DELETE; k++)
	    if (opt[k] < lowest_cost) lowest_cost = opt[k];

	return( lowest_cost );
}

#define MAXLEN 100

main(int argc, char *argv[])
{
     int i;
	 char s[MAXLEN],t[MAXLEN];		/* input strings */

	 s[0] = t[0] = ' ';
	 sscanf(argv[1], "%s",&(s[1]));
	 sscanf(argv[2], "%s",&(t[1]));

     i = string_compare(s, t, strlen(s), strlen(t));
     printf("string1=%s= %d\n", s, strlen(s));
     printf("string2=%s= %d\n", t, strlen(t));
     printf("cost=%d\n", i);
}