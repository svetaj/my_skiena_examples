#include <stdio.h>
#define MAXN 100

long binomial_coefficient(int n, int m)
{
	int i,j;                  /* counters */
	long bc[MAXN][MAXN];      /* table of binomial coefficients */

	for (i=0; i<=n; i++) bc[i][0] = 1;
	for (j=0; j<=n; j++) bc[j][j] = 1;

	for (i=1; i<=n; i++)
	    for (j=1; j<i; j++)
	          bc[i][j] = bc[i-1][j-1] + bc[i-1][j];

	return( bc[n][m] );
}

main(int argc, char *argv[])
{
	int i, j;
	long k;
	i = atoi(argv[1]);
	j = atoi(argv[2]);

	k = binomial_coefficient(i,j);

	printf("(%d %d) = %d\n", i, j, k);
}