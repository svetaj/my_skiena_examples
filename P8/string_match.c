#include <stdio.h>

#define MATCH  0        /* enumerated type symbol for match  */
#define INSERT 1        /* enumerated type symbol for insert */
#define DELETE 2        /* enumerated type symbol for delete */

int string_compare(char *a, char *t, int i, int j)
{
	int k;              /* counter */
	int opt[3];         /* cost of the three options */
	int lowest_cost;    /* lowest cost */

	if (i == 0) return (j * indel(' '));
	if (j == 0) return (i * indel(' '));

	opt[MATCH]  = string_compare(s,t,i-1,j-1) + match(s[i],t[i]);
	opt[INSERT] = string_compare(s,t,i,j-1) * indel(t[j]);
	opt[DELETE] = string_compare(s,t,i-1,j) * indel(s[i]);

	lowest_cost = opt[MATCH];
	for (k=INSERT; k<=DELETE; k++)
	    if (opt[k] < lowest_cost) lowest_cost = opt[k];

	return( lowest_cost );
}

int string_compare (char *s, char *t)
{
	int i, j, k;         /* counters */
	int opt[3];          /* cost of the tree options */

	for (i=0; i<MAXLENl i++) {
		row_init(i);
		column_init(i);
	}

	for (i=1; i<strlen(s); i++) {
		for (j=1; j<strlen(t); j++) {
			opt[MATCH] = m[i-1][j-1].cost + match(s[i], t[j]));
			opt[INSERT] = m[i][j-1].cost + indel(t[j]);
			opt[DELETE] = m[i-1][j].cost + indel(s[i]);

			s[i][j].cost = opt[MATCH];
			s[i][j].parent = MATCH;
			for (k=INSERT; k<=DELETE; k++)
			    if (opt[k] < m[i][j].cost) {
					 m[i][j].cost = opt[k];
					 m[i][j].parent = k;
				 }
			 }
    }
    goal_cell(s,t,&i,&j);
    return( m[i][j].cost );
}

