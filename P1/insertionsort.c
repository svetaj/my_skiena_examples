#include <stdio.h>
typedef int item;

swap(item *x, item *y)
{
	item t;
	t = *x;
	*x = *y;
	*y = t;
}

insertion_sort(item s[], int n)
{
     int i,j;         /* counters */
     for (i=1; i<n; i++) {
        j=i;
        while ((j>0) && (s[j] < s[j-1])) {
            swap (&s[j], &s[j-1]);
            j=j-1;
        }
     }
}

void main(int argc, char *argv[])
{
	int i,j;
	item o[100];

    printf("--------------------------\n");
    printf("------ UNSORTED ----------\n");
    printf("--------------------------\n");
	for (i=1; i<argc; i++) {
       o[i-1] = atoi(argv[i]);
	   printf("%d ",o[i-1]);
	}
    printf("\n--------------------------\n");
    printf("-------- SORTED ----------\n");
    printf("--------------------------\n");

    insertion_sort(o,argc-1);

	for (i=1; i<argc; i++) {
	   printf("%d ",o[i-1]);
	}
}