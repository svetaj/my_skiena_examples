class InsertionSort
{
     static char[] insertion_sort(char[] s, int n)
     {
        int i,j;
        char t;
        for (i=1; i<n; i++) {
           j=i;
           while ((j>0) && (s[j] < s[j-1])) {
//               swap (s[j], s[j-1]);
               t=s[j];
               s[j]=s[j-1];
               s[j-1]=t;
               j=j-1;
               System.out.println(String.copyValueOf(s));
           }
        }
        return s;
     }

     public static void main (String[] args)
     {
          char[] s1 = args[0].toCharArray();
          System.out.println(String.copyValueOf(s1));
          char[] s2 = insertion_sort(s1,s1.length);
//          System.out.println(String.copyValueOf(s2));
     }
}