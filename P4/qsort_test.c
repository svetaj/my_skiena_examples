#include <stdlib.h>
#include <stdio.h>

int intcompare(int *i, int *j)
{
	if (*i > *j) return(1);
	if (*i < *j) return(-1);
	return(0);
}

void main(int argc, char *argv[])
{
	int i,j;
	int o[100];

    printf("--------------------------\n");
    printf("------ UNSORTED ----------\n");
    printf("--------------------------\n");
	for (i=1; i<argc; i++) {
       o[i-1] = atoi(argv[i]);
	   printf("%d ",o[i-1]);
	}
    printf("\n--------------------------\n");
    printf("-------- SORTED ----------\n");
    printf("--------------------------\n");

    qsort(o,argc-1,sizeof(int), intcompare);

	for (i=1; i<argc; i++) {
	   printf("%d ",o[i-1]);
	}
}