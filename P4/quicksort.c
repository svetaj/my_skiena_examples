#include <stdlib.h>
#include <stdio.h>

typedef int item_type;

swap(item_type *x, item_type *y)
{
	item_type t;
	t = *x;
	*x = *y;
	*y = t;
}

quicksort(item_type s[], int l, int h)
{
	int p;            /* index of partition */

    if ((h-l) > 0) {
		p = partition(s,l,h);
		quicksort(s,l,p-1);
		quicksort(s,p+1,h);
    }
}

int partition(item_type s[], int l, int h)
{
	int i;                  /* counter */
	int p;                  /* pivot element index */
	int firsthigh;          /* divider position for pivot element */

	p = h;
	firsthigh = l;
	for (i=l; i<h; i++)
	    if (s[i] < s[p]) {
			swap(&s[i], &s[firsthigh]);
			firsthigh++;
	    }
	swap(&s[p], &s[firsthigh]);

	return(firsthigh);
}

void main(int argc, char *argv[])
{
	int i,j;
	item_type o[100];

    printf("--------------------------\n");
    printf("------ UNSORTED ----------\n");
    printf("--------------------------\n");
	for (i=1; i<argc; i++) {
       o[i-1] = atoi(argv[i]);
	   printf("%d ",o[i-1]);
	}
    printf("\n--------------------------\n");
    printf("-------- SORTED ----------\n");
    printf("--------------------------\n");

    quicksort(o,0,argc-2);

	for (i=1; i<argc; i++) {
	   printf("%d ",o[i-1]);
	}
}