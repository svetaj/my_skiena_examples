#include <stdlib.h>
#include <stdio.h>

mergesort(item_type s[], int low, int high)
{
	int i;            /* counter */
	int middle;       /* index of middle element */

	if (low < high) {
		middle = (low+high)/2;
		mergesort(s, low, middle);
		mergesort(s, middle+1, high);
		merge(s, low, middle, high);
    }
}

merge(item_type s[], int low, int middle, int high)
{
	int i;                           /* counter */
	queue buffer1, buffer2;          /* buffers to hold elements for merging */

	init_queue(&buffer1);
	init_queue(&buffer2);

	for (i=low; i<=middle; i++) enqueue(&buffer1, s[i]);
	for (i=middle+1; i<=high; i++) enqueue(&buffer2, s[i]);

	i=low;
	while(!(empty_queue(&buffer1) || empty_queue(&buffer2))) {
		if (headq(&buffer1) <= headq(&buffer2))
		    s[i++] = dequeue(&buffer1);
		else
		    s[i++] = dequeue(&buffer2);
    }
    while(!empty_queue(&buffer1)) s[i++] = dequeue(&buffer1);
    while(!empty_queue(&buffer2)) s[i++] = dequeue(&buffer2);
}

void main(int argc, char *argv[])
{
	int i,j;
	item_type o[100];

    printf("--------------------------\n");
    printf("------ UNSORTED ----------\n");
    printf("--------------------------\n");
	for (i=1; i<argc; i++) {
       o[i-1] = atoi(argv[i]);
	   printf("%d ",o[i-1]);
	}
    printf("\n--------------------------\n");
    printf("-------- SORTED ----------\n");
    printf("--------------------------\n");

    mergesort(o,0,argc-2);

	for (i=1; i<argc; i++) {
	   printf("%d ",o[i-1]);
	}
}