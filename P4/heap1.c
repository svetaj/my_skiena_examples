#include <stdio.h>
#define PQ_SIZE 100

typedef int item_type;

typedef struct {
	item_type q[PQ_SIZE+1];    /* body of queue */
	int n;                     /* number of queue elements */
} priority_queue;

pq_parent(int n)
{
	if (n == 1) return(-1);
	else return((int) n/2);    /* implicitly take floor(n/2) */
}

pq_young_child(int n)
{
	return(2 * n);
}

pq_swap (priority_queue *q, int p1, int p2)
{
	item_type x;
	x = q->q[p1];
	q->q[p1] = q->q[p2];
	q->q[p2] = x;
}

pq_insert(priority_queue *q, item_type x)
{
	if (q->n >= PQ_SIZE)
	    printf("Warning: priority queue overflow insert x=%d\n", x);
	else {
	    q->n = (q->n) + 1;
	    q->q[ q->n ] = x;
	    pq_print(q,"insert");
	    bubble_up(q, q->n);
    }
}

bubble_up(priority_queue *q, int p)
{
	 if (pq_parent(p) == -1) return;    /* at root of heap, no parent */

	 if (q->q[pq_parent(p)] > q->q[p]) {
		 pq_swap(q, p, pq_parent(p));
 	     pq_print(q,"bubble_up");
		 bubble_up(q, pq_parent(p));
     }
}

pq_init(priority_queue *q)
{
	q->n = 0;
}

pq_print(priority_queue *q, char *h)
{
	int i,j,k;

	printf("------------------- %s -------------------\n", h);
	for (i=1; i<=q->n; i++) {
	    printf("%d ",q->q[i]);
	    k=2;
	    for (j=0; j<=i; j++) {
	       if (i == k-1) {
			   printf("\n");
			   break;
	       }
	       k = k*2;
	    }
    }
	printf("\n");
	printf("-------------------------------------------\n");
}

/*
make_heap(priority_queue *q, item_type s[], int n)
{
	int i;
	pq_init(q);
	for (i=0; i<n; i++)
	    pq_insert(q, s[i]);
}
*/

make_heap(priority_queue *q, item_type s[], int n)
{
	int i;                /* counter */
	q->n = n;
	for (i=0; i<n; i++) q->q[i+1] = s[i];

	for (i=q->n; i>=1; i--) bubble_down(q,i);
}

item_type extract_min(priority_queue *q)
{
	int min = -1;                   /* minimum value */

	if (q->n <= 0) printf("Warning: empty priority queue\n");
    else {
		min = q->q[1];
		q->q[1] = q->q[ q->n ];
		pq_print(q, "extract_min");
		q->n = q->n - 1;
		bubble_down (q, 1);
    }

    return (min);
}


bubble_down (priority_queue *q, int p)
{
    int c;                    /* child index */
    int i;                    /* counter */
    int min_index;            /* index of lightest child */

    c = pq_young_child(p);
    min_index = p;

    for (i=0; i<=1; i++)
        if ((c+i) <= q->n) {
			if (q->q[min_index] > q->q[c+i]) min_index = c+i;
	    }


	if (min_index != p) {
		pq_swap(q, p, min_index);
 	    pq_print(q,"bubble_down");
		bubble_down(q, min_index);
    }
}

heapsort(item_type s[], int n)
{
	int i;                /* counters */
	priority_queue q;     /* heap for the sort */

    make_heap(&q, s, n);
    pq_print(&q, "make_heap");

    for (i=0; i<n; i++)
        s[i] = extract_min(&q);
}

void main(int argc, char *argv[])
{
	int i,j;
	int o[100];

    printf("--------------------------\n");
    printf("------ UNSORTED ----------\n");
    printf("--------------------------\n");
	for (i=1; i<argc; i++) {
       o[i-1] = atoi(argv[i]);
	   printf("%d ",o[i-1]);
	}
    printf("\n--------------------------\n");
    printf("-------- SORTED ----------\n");
    printf("--------------------------\n");

    heapsort(o,argc-1);

	for (i=1; i<argc; i++) {
	   printf("%d ",o[i-1]);
	}
}